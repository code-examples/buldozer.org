# [buldozer.org](https://buldozer.org) source codes

<br/>

### Run buldozer.org on localhost

    # vi /etc/systemd/system/buldozer.org.service

Insert code from buldozer.org.service

    # systemctl enable buldozer.org.service
    # systemctl start buldozer.org.service
    # systemctl status buldozer.org.service

http://localhost:4059
